## Documentation for the Batch AD project

This project will serve as a warehouse for keeping all the documents related to the Batch Anomaly Detection project. Find the explanation of all the subgroups of this project bellow.

### SWAN 

#### Job Data Pipeline

This project houses the notebook that attempted to use SWAN to create a job number time-series metric from the JobEvent data used by consuming the Condors' schedds logs.

### Kubeflow

In this subgroup the projects needed to create an ADMON pipeline are stored. Contact the [ADMON](https://admon.docs.cern.ch/) team for more info on that.
